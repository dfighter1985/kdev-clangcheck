/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "parser.h"
#include <QRegExp>

using namespace KDevelop;

namespace ClangCheck
{

Parser::Parser()
{
}

Parser::~Parser()
{
}

void Parser::setInput(const QString &in)
{
    /// Break up the input into lines
    m_lines = in.split('\n');
}

void Parser::parse()
{
    while (m_lines.size() > 0) {
        parseLine(m_lines.front());
        m_lines.pop_front();
    }
}

void Parser::parseLine(const QString &line)
{
    /**
     * @brief Check if the line contains a message
     *
     * Example: /one.cpp:23:9: warning: Value stored to 'b' during its initialization is never read
     */
    QRegExp regExp("^.*\\.[0-9a-zA-Z]*:[0-9]*:[0-9]*: [a-zA-Z ]*:");
    int idx = line.indexOf(regExp);
    if (idx == -1)
        return;

    QStringList parts = line.split(':');

    IProblem::Ptr problem(new DetectedProblem());

    DocumentRange range;
    range.document = IndexedString(parts[0]);
    range.setBothLines(parts[1].toInt() - 1);
    range.setBothColumns(parts[2].toInt() - 1);

    problem->setFinalLocation(range);
    problem->setDescription(parts[4].trimmed());
    problem->setExplanation(problem->description());

    QString severity = parts[3].trimmed();
    if (severity == "warning")
        problem->setSeverity(IProblem::Warning);
    else
        problem->setSeverity(IProblem::Error);

    problem->setSource(IProblem::Plugin);

    m_problems.push_back(problem);
}

}

