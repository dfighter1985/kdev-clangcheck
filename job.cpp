/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#include "job.h"
#include <QStringList>
#include "parser.h"

#include <interfaces/iproject.h>
#include <interfaces/iprojectcontroller.h>
#include <project/projectmodel.h>
#include <kdevelop/custom-definesandincludes/idefinesandincludesmanager.h>

#include <QTextStream>

using namespace KDevelop;

namespace
{
const QString defaultPath = QStringLiteral("/usr/bin/clang");
}

namespace ClangCheck
{

QStringList getDefines(IProject *project, const QString &path)
{
    QList<ProjectFileItem*> items = project->filesForPath(IndexedString(path));

    QStringList defines;

    foreach (ProjectFileItem *item, items) {
        QHash<QString, QString> definePairs = IDefinesAndIncludesManager::manager()->defines(item, IDefinesAndIncludesManager::ProjectSpecific);

        QHashIterator<QString, QString> itr(definePairs);
        while (itr.hasNext()) {
            itr.next();
            if (!itr.value().isEmpty())
                defines.push_back(QStringLiteral("-D") + itr.key() + QStringLiteral("=") + itr.value());
            else
                defines.push_back(QStringLiteral("-D") + itr.key());
        }
    }

    return defines;
}

QStringList getIncludePaths(IProject *project, const QString &path)
{
    QStringList paths;

    QList<ProjectFileItem*> items = project->filesForPath(IndexedString(path));

    foreach (ProjectFileItem *item, items) {
        Path::List includes = IDefinesAndIncludesManager::manager()->includes(item);

        foreach (const Path &includePath, includes) {
            paths.push_back(includePath.toLocalFile());
        }
    }

    return paths;
}


Job::Job(const QString &path, QObject *parent)
    : KJob(parent)
    , m_process(new QProcess(this))
    , m_parser(new Parser())
{
    setCapabilities(Killable);

    KConfigGroup globalGroup = KSharedConfig::openConfig()->group("ClangCheck");

    m_params.filePath = path;
    m_params.executablePath = globalGroup.readEntry("Clang Path", defaultPath);
    if (m_params.executablePath.isEmpty())
        m_params.executablePath = defaultPath;

    IProject *project = ICore::self()->projectController()->findProjectForUrl(QUrl::fromLocalFile(path));
    if (project != nullptr) {
        KConfigGroup projectGroup = project->projectConfiguration()->group("ClangCheck");

        m_params.additionalArguments = projectGroup.readEntry("AdditionalArguments", QString());
        m_params.defines = getDefines(project, path);
        m_params.includePaths = getIncludePaths(project, path);
    }
}

Job::~Job()
{
}

void Job::start()
{
    QStringList arguments;
    arguments.push_back("--analyze");
    arguments.push_back(m_params.additionalArguments);

    foreach (const QString &define, m_params.defines) {
        arguments.push_back(define);
    }

    foreach (const QString &includePath, m_params.includePaths) {
        arguments.push_back("-I" + includePath);
    }

    arguments.push_back(m_params.filePath);

    m_process->setProgram(m_params.executablePath);
    m_process->setArguments(arguments);

    connect(m_process.data(), SIGNAL(error(QProcess::ProcessError)), this, SLOT(onProcessError(QProcess::ProcessError)));
    connect(m_process.data(), SIGNAL(finished(int)), this, SLOT(onProcessFinished(int)));

    m_process->start();
}

Parser* Job::parser() const
{
    return m_parser.data();
}

bool Job::doKill()
{
    m_process->kill();
    return true;
}

void Job::onProcessError(QProcess::ProcessError error)
{
    switch (error) {
        case QProcess::FailedToStart: setError(ProcessFailedToStart); break;
        case QProcess::Crashed: setError(ProcessCrashed); break;
        case QProcess::UnknownError: setError(UnknownError); break;
        default: break;
    }

    emitResult();
}

void Job::onProcessFinished(int exitCode)
{
    Q_UNUSED(exitCode);

    QString error = m_process->readAllStandardError();
    QTextStream(stdout) << error;
    m_parser->setInput(error);
    m_parser->parse();

    emitResult();
}

}
