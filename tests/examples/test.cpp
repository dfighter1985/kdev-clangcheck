/*
 * Copyright 2015 Laszlo Kis-Adam
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

// This is just a test file for clang-check

#include <cstdio>

void insecure()
{
    char s[1024];
    gets(s);
}

void useAfterFree()
{
    int *a = new int[100];
    delete[] a;
    int b = a[0];
}

void wrongDelete()
{
    int *a = new int[100];
    delete a;
}

void doubleDelete()
{
    int *a = new int[100];
    delete[] a;
    delete[] a;
}
