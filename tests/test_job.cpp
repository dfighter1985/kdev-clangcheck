/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <QTest>
#include <QSignalSpy>

#include <kdevplatform/tests/testcore.h>
#include <kdevplatform/tests/autotestshell.h>
#include <interfaces/iruncontroller.h>

#include <KSharedConfig>
#include <KConfigGroup>

#include "../job.h"
#include "../parser.h"

using namespace KDevelop;
using namespace ClangCheck;

namespace
{

const QString testFile = QStringLiteral("examples/test.cpp");

struct ProblemData
{
    QString file;
    int line;
    int column;
    IProblem::Severity severity;
    QString msg;
};

const ProblemData testProblems[] = {
    { testFile, 27, 5, IProblem::Warning, QStringLiteral("Call to function 'gets' is extremely insecure as it can always result in a buffer overflow") },
    { testFile, 34, 9, IProblem::Warning, QStringLiteral("Value stored to 'b' during its initialization is never read") },
    { testFile, 34, 13, IProblem::Warning, QStringLiteral("Use of memory after it is freed") },
    { testFile, 40, 5, IProblem::Warning, QStringLiteral("Memory allocated by 'new[]' should be deallocated by 'delete[]', not 'delete'") },
    { testFile, 47, 5, IProblem::Warning, QStringLiteral("Attempt to free released memory") },
};

#define MYCOMPARE(actual, expected) \
    if (!QTest::qCompare(actual, expected, #actual, #expected, __FILE__, __LINE__)) \
    return false

// Compares the result problems to the reference problems
bool checkProblems(const QVector<IProblem::Ptr> &problems)
{
    int c = sizeof(testProblems) / sizeof(ProblemData);

    MYCOMPARE(problems.count(), c);

    for (int i = 0; i < c; i++) {
        MYCOMPARE(problems[i]->finalLocation().document.str(), testProblems[i].file);
        MYCOMPARE(problems[i]->finalLocation().start().line() + 1, testProblems[i].line);
        MYCOMPARE(problems[i]->finalLocation().start().column() + 1, testProblems[i].column);
        MYCOMPARE(problems[i]->severity() == testProblems[i].severity, true);
        MYCOMPARE(problems[i]->description(), testProblems[i].msg);
    }

    return true;
}

}

class TestJob : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();
    void testJobWrongExecutable();
    void testJob();

private:

};

void TestJob::initTestCase()
{
    AutoTestShell::init();
    TestCore::initialize(Core::NoUi);
}

void TestJob::cleanupTestCase()
{
    TestCore::shutdown();
}

void TestJob::testJobWrongExecutable()
{
    KConfigGroup group = KSharedConfig::openConfig()->group("ClangCheck");
    group.writeEntry(QStringLiteral("Clang Path"), QStringLiteral("/usr/local/yada/yada/wrong"));

    Job *job = new Job(testFile, this);
    QSignalSpy spy(job, &Job::finished);
    ICore::self()->runController()->registerJob(job);

    spy.wait();

    QVERIFY(job->error() == Job::ProcessFailedToStart);
}

void TestJob::testJob()
{
    KConfigGroup group = KSharedConfig::openConfig()->group("ClangCheck");
    group.writeEntry(QStringLiteral("Clang Path"), QStringLiteral(""));

    Job *job = new Job(testFile, this);
    QSignalSpy spy(job, &Job::finished);
    ICore::self()->runController()->registerJob(job);

    spy.wait();

    QVERIFY(job->error() == KJob::NoError);

    QVector<IProblem::Ptr> problems = job->parser()->problems();
    if (!checkProblems(problems))
        return;
}

QTEST_MAIN(TestJob)

#include "test_job.moc"
