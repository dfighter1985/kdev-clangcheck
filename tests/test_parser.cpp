/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <QTest>
#include "parser.h"

#include <kdevplatform/interfaces/iproblem.h>
#include <kdevplatform/tests/testcore.h>
#include <kdevplatform/tests/autotestshell.h>
#include <kdevplatform/serialization/indexedstring.h>

using namespace KDevelop;
using namespace ClangCheck;


namespace
{

struct ProblemData
{
    QString file;
    int line;
    int col;
    QString severity;
    QString msg;
};

ProblemData data[] = {
    {QStringLiteral("one.cpp"), 23, 9, QStringLiteral("warning"), QStringLiteral("A simple warning")},
    {QStringLiteral("two.cpp"), 15, 1, QStringLiteral("error"), QStringLiteral("Just an 'error'")},
    {QStringLiteral("three.cpp"), 12, 2, QStringLiteral("fatal error"), QStringLiteral("This is a 'fatal' error")},
};

QString makeLine(const ProblemData &d)
{
    QString s;

    s += d.file;
    s += ':';
    s += QString::number(d.line);
    s += ':';
    s += QString::number(d.col);
    s += ": ";
    s += d.severity;
    s += ": ";
    s += d.msg;
    s += '\n';

    return s;
}

}

class TestParser : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();

    void testParsing();

private:
    QString generateInput();

    QScopedPointer<Parser> m_parser;
    QVector<ProblemData> m_problems;
};

void TestParser::initTestCase()
{
    AutoTestShell::init();
    TestCore::initialize(Core::NoUi);

    m_parser.reset(new Parser());
}

void TestParser::cleanupTestCase()
{
    TestCore::shutdown();
}

void TestParser::testParsing()
{
    // Generate input and then parse it
    QString input = generateInput();
    m_parser->setInput(input);
    m_parser->parse();

    QVector<IProblem::Ptr> problems = m_parser->problems();

    // Compare the results to what we generated from
    int problemCount = sizeof(data) / sizeof(ProblemData);
    QCOMPARE(problems.count(), problemCount);

    for (int i = 0; i < problemCount; i++) {
        QCOMPARE(problems[i]->description(), data[i].msg);
    }
}

QString TestParser::generateInput()
{
    QString input;

    input.append(makeLine(data[0]));
    input.append(QStringLiteral("\n"));
    input.append(QStringLiteral("Yada Yada\n"));
    input.append(makeLine(data[1]));
    input.append(QStringLiteral("blah\n"));
    input.append(QStringLiteral("bleh\n"));
    input.append(makeLine(data[2]));
    input.append(QStringLiteral("This is the end...\n"));

    return input;
}

QTEST_MAIN(TestParser)

#include "test_parser.moc"

